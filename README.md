# YAIG

A simple gallery which interfaces with the Flicker API accessing the public feed.

### Libraries

YAIG usese the following open source libraries:

* Retrofit 2
* OkHTTP 3
* ReactiveX
* RxJava
* RxAndroid
* Butterknife
* Fresco

### Todos

* Save image to gallery
* Search by tag
* Unit Testing
* Open in System Browser