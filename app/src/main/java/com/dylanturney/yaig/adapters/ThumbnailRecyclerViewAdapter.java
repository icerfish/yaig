package com.dylanturney.yaig.adapters;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dylanturney.yaig.R;
import com.dylanturney.yaig.api.models.Entry;
import com.dylanturney.yaig.fragments.GalleryFragment;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Entry} and makes a call to the
 * specified {@link GalleryFragment.OnListFragmentInteractionListener}.
 */
public class ThumbnailRecyclerViewAdapter extends RecyclerView.Adapter<ThumbnailRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = ThumbnailRecyclerViewAdapter.class.getSimpleName();

    private final List<Entry> mValues;
    private final GalleryFragment.OnListFragmentInteractionListener mListener;

    public ThumbnailRecyclerViewAdapter(List<Entry> entries, GalleryFragment.OnListFragmentInteractionListener listener) {
        mValues = entries;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_thumbnail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mTitle.setText(mValues.get(position).getTitle());
        String url = holder.mItem.getLink().get(holder.mItem.getLink().size() - 1).getHref();
        holder.mSimpleDraweeView.setImageURI( // Grab the last link and display the image
                Uri.parse(url)
        );

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View mView;
        @BindView(R.id.title)
        TextView mTitle;
        @BindView(R.id.sdvImage)
        SimpleDraweeView mSimpleDraweeView;
        public Entry mItem;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            mView = view;
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText() + "'";
        }
    }
}
