package com.dylanturney.yaig.api.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

import io.realm.RealmObject;

/**
 * Created by dylanturney on 16/11/2016.
 */

@Root
public class Generator extends RealmObject {

    @Text
    private String value;

    @Attribute(name="uri", required = false)
    private String uri;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUri() {
        return uri;
    }
}
