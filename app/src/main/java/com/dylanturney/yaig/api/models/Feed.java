package com.dylanturney.yaig.api.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by dylanturney on 16/11/2016.
 */

@Root
public class Feed extends RealmObject {

    @PrimaryKey
    @Element
    private String id;

    @Element
    private String icon;

    @Element
    private String title;

    @Element
    private String updated;

    @ElementList(entry = "link", inline = true)
    private RealmList<Link> link;

    @ElementList(entry = "entry", inline = true)
    private RealmList<Entry> entry;

    @Element(required = false)
    private String subtitle;

    @Element
    private Generator generator;

    @Attribute(name = "xmlns", required = false)
    private String xmlns;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public RealmList<Link> getLink() {
        return link;
    }

    public void setLink(RealmList<Link> link) {
        this.link = link;
    }

    public RealmList<Entry> getEntry() {
        return entry;
    }

    public void setEntry(RealmList<Entry> entry) {
        this.entry = entry;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public Generator getGenerator() {
        return generator;
    }

    public void setGenerator(Generator generator) {
        this.generator = generator;
    }

    public String getXmlns() {
        return xmlns;
    }

    public void setXmlns(String xmlns) {
        this.xmlns = xmlns;
    }

}
