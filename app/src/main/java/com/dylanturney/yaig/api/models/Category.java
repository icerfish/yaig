package com.dylanturney.yaig.api.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import io.realm.RealmObject;

/**
 * Created by dylanturney on 16/11/2016.
 */

@Root
public class Category extends RealmObject {

    @Attribute
    private String scheme;

    @Attribute
    private String term;

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

}
