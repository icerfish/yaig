package com.dylanturney.yaig.api.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import io.realm.RealmObject;

/**
 * Created by dylanturney on 16/11/2016.
 */

@Root
public class Author extends RealmObject {

    @Element
    private String name;

    @Element
    private String uri;

    @Element
    private String nsid;

    @Element
    private String buddyicon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getNsid() {
        return nsid;
    }

    public void setNsid(String nsid) {
        this.nsid = nsid;
    }

    public String getBuddyicon() {
        return buddyicon;
    }

    public void setBuddyicon(String buddyicon) {
        this.buddyicon = buddyicon;
    }
}
