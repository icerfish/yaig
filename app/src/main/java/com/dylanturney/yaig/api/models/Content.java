package com.dylanturney.yaig.api.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

import io.realm.RealmObject;

/**
 * Created by dylanturney on 16/11/2016.
 */

@Root
public class Content extends RealmObject {

    @Text
    private String value;

    @Attribute
    private String type;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
