package com.dylanturney.yaig.api;

import com.dylanturney.yaig.api.models.Feed;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by dylanturney on 16/11/2016.
 */

public interface FlickrService {

    @GET("/services/feeds/photos_public.gne")
    Observable<Feed> getFeed();
}
