package com.dylanturney.yaig.api.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import io.realm.RealmObject;

/**
 * Created by dylanturney on 16/11/2016.
 */

@Root
public class Link extends RealmObject {

    @Attribute(name = "rel", required = false)
    private String rel;

    @Attribute(name = "type", required = false)
    private String type;

    @Attribute(name = "href", required = false)
    private String href;

    public String getRel() {
        return rel;
    }

    public void setRel(String rel) {
        this.rel = rel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

}
