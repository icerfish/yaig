package com.dylanturney.yaig.api;

import com.dylanturney.yaig.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by dylanturney on 16/11/2016.
 */

public class Api {

    private static Retrofit sInstance;
    private static FlickrService sApi;

    static final String API_ENDPOINT = "https://api.flickr.com";

    static {
        sInstance = buildRetrofit(API_ENDPOINT, getHttpClient());

        sApi = sInstance.create(FlickrService.class);
    }

    private static HttpLoggingInterceptor getInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ?
                HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

        return interceptor;
    }

    private static OkHttpClient getHttpClient() {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(21, TimeUnit.SECONDS)
                .readTimeout(21, TimeUnit.SECONDS)
                .addInterceptor(getInterceptor())
                .followRedirects(false)
                .followSslRedirects(false).build();

        return client;
    }

    private static Retrofit buildRetrofit(String endpoint, OkHttpClient client) {
        return new Retrofit.Builder()
                .baseUrl(endpoint)
                .client(client)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    // Returns Retrofit Instance
    public static Retrofit getsRetrofitInstance() {
        return sInstance;
    }

    // Returns FlickrService for making API calls
    public static FlickrService getApi() {
        return sApi;
    }

}
