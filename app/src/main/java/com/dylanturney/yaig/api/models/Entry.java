package com.dylanturney.yaig.api.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by dylanturney on 16/11/2016.
 */

@Root(strict = false)
public class Entry extends RealmObject {

    @PrimaryKey
    @Element
    private String id;

    @Element
    private Content content;

    @Element
    private Author author;

    @Element
    private String title;

    @Element(name = "catergory", required = false)
    private Category category;

    @Element
    private String updated;

    @ElementList(entry = "link", inline = true)
    private RealmList<Link> link;

    @Element
    private String published;

    @Element
    private String displaycategories;

    @Namespace(reference = "urn:flickr:user", prefix = "flickr")
    @Element(name = "date_taken", required = false)
    private String dateTaken;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public List<Link> getLink() {
        return link;
    }

    public void setLink(RealmList<Link> link) {
        this.link = link;
    }

    public String getPublished() {
        return published;
    }

    public void setPublished(String published) {
        this.published = published;
    }

    public String getDisplaycategories() {
        return displaycategories;
    }

    public void setDisplaycategories(String displaycategories) {
        this.displaycategories = displaycategories;
    }

    public String getDateTaken() {
        return dateTaken;
    }

    public void setDateTaken(String dateTaken) {
        this.dateTaken = dateTaken;
    }
}
