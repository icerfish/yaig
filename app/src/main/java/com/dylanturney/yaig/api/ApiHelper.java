package com.dylanturney.yaig.api;

import com.dylanturney.yaig.api.models.Entry;
import com.dylanturney.yaig.api.models.Feed;

import io.realm.Realm;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by dylanturney on 16/11/2016.
 */

public class ApiHelper {

    public static final String TAG = ApiHelper.class.getSimpleName();

    public static Subscription getFeed(final Realm realm, final Callback<Feed> callback) {
        return Api.getApi().getFeed()
                .subscribeOn(Schedulers.newThread())  // Subscribe on a new thread
                .observeOn(AndroidSchedulers.mainThread()) // Observe on the main thread
                .subscribe(new Action1<Feed>() {
                    @Override // On Successful Response
                    public void call(Feed feed) {
                        realm.beginTransaction();
                        realm.where(Entry.class).findAll().deleteAllFromRealm();
                        realm.copyToRealmOrUpdate(feed.getEntry());
                        realm.commitTransaction();
                        callback.onComplete(feed);
                    }
                }, new Action1<Throwable>() {
                    @Override // On Error
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        callback.onError(throwable);
                    }
                });
    }

    public interface Callback<T> {
        // Api call complete
        void onComplete(T t);

        // Api error
        void onError(Throwable e);
    }

}
