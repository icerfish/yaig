package com.dylanturney.yaig.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.dylanturney.yaig.R;
import com.dylanturney.yaig.api.models.Entry;
import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.Priority;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import butterknife.BindView;

public class PhotoActivity extends BaseActivity {

    public static final String EXTRA_ID = "ID";
    public static final int REQUEST_WRITE_STORAGE = 100;

    @BindView(R.id.sdvImage)
    SimpleDraweeView sdvImage;

    private Entry mEntry;
    private String mUrl;
    private State mState = State.SHARE;

    {
        mUseButterKnife = true;
        mLayoutId = R.layout.activity_photo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent() != null && getIntent().hasExtra(EXTRA_ID)) {
            mEntry = mRealm.copyFromRealm(
                    mRealm.where(Entry.class).equalTo(
                            "id", getIntent().getStringExtra(EXTRA_ID)
                    ).findFirst()
            );
        }

        if (mEntry != null) {
            mUrl = mEntry.getLink().get(mEntry.getLink().size() - 1).getHref();
            sdvImage.setImageURI(Uri.parse(mUrl));
            getSupportActionBar().setTitle(mEntry.getTitle());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_photo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.share:
                mState = State.SHARE;
                checkStoragePermission();
                return true;
            case R.id.metadata:
                showMetadata();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showMetadata() {
        new AlertDialog.Builder(this)
                .setTitle("Metadata")
                .setMessage(createMetadataString())
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .show();
    }

    private String createMetadataString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Title: ");
        sb.append(mEntry.getTitle());
        sb.append(System.getProperty("line.separator"));
        sb.append("Date Taken: ");
        sb.append(mEntry.getDateTaken());
        sb.append(System.getProperty("line.separator"));
        sb.append("Date Published: ");
        sb.append(mEntry.getPublished());
        sb.append(System.getProperty("line.separator"));
        sb.append("Date Updated: ");
        sb.append(mEntry.getUpdated());
        sb.append(System.getProperty("line.separator"));
        sb.append("Author: ");
        sb.append(mEntry.getAuthor().getName());

        return sb.toString();
    }

    private void shareBitmap(Bitmap bitmap) {
        String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                bitmap, "Image Description", null);
        Uri bmpUri = Uri.parse(path);
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
        shareIntent.setType("image/*");

        startActivity(Intent.createChooser(shareIntent, "Share Image"));
    }

    private void getBitmap() {
        if (mUrl == null) {
            return;
        }

        showProgressToast();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();

        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(Uri.parse(mUrl))
                .setRequestPriority(Priority.HIGH)
                .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                .build();

        DataSource<CloseableReference<CloseableImage>> dataSource =
                imagePipeline.fetchDecodedImage(imageRequest, this);

        try {
            dataSource.subscribe(new BaseBitmapDataSubscriber() {
                @Override
                public void onNewResultImpl(@Nullable Bitmap bitmap) {
                    if (bitmap == null) {
                        Log.d(TAG, "Bitmap data source returned success, but bitmap null.");
                        return;
                    }
                    // The bitmap provided to this method is only guaranteed to be around
                    // for the lifespan of this method. The image pipeline frees the
                    // bitmap's memory after this method has completed.
                    //
                    // This is fine when passing the bitmap to a system process as
                    // Android automatically creates a copy.
                    //
                    // If you need to keep the bitmap around, look into using a
                    // BaseDataSubscriber instead of a BaseBitmapDataSubscriber.

                    switch (mState) {
                        case SHARE:
                            shareBitmap(bitmap);
                            break;
                    }
                }

                @Override
                public void onFailureImpl(DataSource dataSource) {
                    // No cleanup required here
                }
            }, CallerThreadExecutor.getInstance());
        } finally {
            if (dataSource != null) {
                dataSource.close();
            }
        }
    }

    private void showProgressToast() {
        String copy = "";
        switch (mState) {
            case SHARE:
                copy = getResources().getString(R.string.sharing);
                break;
        }

        Toast.makeText(this, copy, Toast.LENGTH_SHORT).show();
    }

    private void checkStoragePermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_WRITE_STORAGE);

                // REQUEST_WRITE_STORAGE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            getBitmap();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getBitmap();
                } else {
                    // Show explanation
                    Toast.makeText(this, R.string.permissions_write, Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    public enum State {
        SHARE
    }
}
