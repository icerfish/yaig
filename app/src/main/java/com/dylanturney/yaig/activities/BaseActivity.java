package com.dylanturney.yaig.activities;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.ButterKnife;
import io.realm.Realm;
import rx.Subscription;

/**
 * Created by dylanturney on 16/11/2016.
 *
 * BaseActivity for convenience
 */

public class BaseActivity extends AppCompatActivity {

    protected final String TAG = this.getClass().getSimpleName();

    protected final List<Subscription> mSubscriptions = new ArrayList<>();

    @LayoutRes
    protected int mLayoutId;
    protected boolean mUseButterKnife = false;
    protected Realm mRealm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mUseButterKnife) {
            setContentView(mLayoutId);
            ButterKnife.bind(this);
        }

        mRealm = Realm.getDefaultInstance();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unsubscribe();

        if (mRealm != null) {
            mRealm.close();
        }
    }


    // Unsubscribe and remove all of the subscriptions that haven't done so already
    protected void unsubscribe() {
        for (Iterator<Subscription> iterator = mSubscriptions.iterator(); iterator.hasNext();) {
            Subscription subscription = iterator.next();
            if (subscription != null && !subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
            iterator.remove();
        }
    }

}
