package com.dylanturney.yaig.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.dylanturney.yaig.R;
import com.dylanturney.yaig.api.models.Entry;
import com.dylanturney.yaig.fragments.GalleryFragment;

public class MainActivity extends BaseActivity
        implements GalleryFragment.OnListFragmentInteractionListener {

    private GalleryFragment galleryFragment;

    {
        mUseButterKnife = true;
        mLayoutId = R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        galleryFragment = GalleryFragment.newInstance(2);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.flContainer, galleryFragment).commit();
    }

    @Override
    public void onListFragmentInteraction(Entry entry) {
        if (galleryFragment != null && galleryFragment.isRefreshing()) {
            return; // Return if fetching new data from the server
        }
        Intent intent = new Intent(this, PhotoActivity.class);
        intent.putExtra(PhotoActivity.EXTRA_ID, entry.getId());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.taken:
                if (galleryFragment != null) {
                    galleryFragment.orderByDatePublished(false);
                }
                return true;
            case R.id.published:
                if (galleryFragment != null) {
                    galleryFragment.orderByDatePublished(true);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
