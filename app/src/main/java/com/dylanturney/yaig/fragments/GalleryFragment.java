package com.dylanturney.yaig.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dylanturney.yaig.R;
import com.dylanturney.yaig.adapters.ThumbnailRecyclerViewAdapter;
import com.dylanturney.yaig.api.ApiHelper;
import com.dylanturney.yaig.api.models.Entry;
import com.dylanturney.yaig.api.models.Feed;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.realm.Sort;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class GalleryFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.list)
    RecyclerView mRecyclerView;
    @BindView(R.id.srlRefresh)
    SwipeRefreshLayout mRefresh;

    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private List<Entry> mEntries = new ArrayList<>();
    private ThumbnailRecyclerViewAdapter mAdapter;
    private boolean mOrderByDatePublished = true;

    {
        mUseButterKnife = true;
        mLayoutId = R.layout.fragment_thumbnail_list;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public GalleryFragment() {

    }

    @SuppressWarnings("unused")
    public static GalleryFragment newInstance(int columnCount) {
        GalleryFragment fragment = new GalleryFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = super.onCreateView(inflater, container, savedInstanceState);

        if (mColumnCount <= 1) {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        } else {
            mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, mColumnCount));
        }

        mRefresh.setOnRefreshListener(this);
        mAdapter = new ThumbnailRecyclerViewAdapter(mEntries, mListener);
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();

        reloadData(false);
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mRefresh.isRefreshing()) {
            mRefresh.setRefreshing(false);
        }
    }

    // Set fetch to true if you want to fetch from the server
    private void reloadData(boolean fetch) {
        // Add subscription to list to keep track of
        if (fetch) {
            mSubscriptions.add(ApiHelper.getFeed(mRealm, new ApiHelper.Callback<Feed>() {
                @Override
                public void onComplete(Feed feed) {
                    mEntries.clear();
                    mEntries.addAll(feed.getEntry());

                    mAdapter.notifyDataSetChanged();

                    mRefresh.setRefreshing(false);
                }

                @Override
                public void onError(Throwable e) {

                    mRefresh.setRefreshing(false);
                }
            }));
        } else {
            mEntries.clear();
            // Search Realm first for existence of Entries and sort by selected date
            mEntries.addAll(mRealm.copyFromRealm(mRealm.where(Entry.class)
                    .findAllSorted(mOrderByDatePublished
                            ? "published" : "dateTaken", Sort.DESCENDING)
            ));

            // If entries are empty fetch from server
            if (mEntries.isEmpty()) {
                mRefresh.setRefreshing(true);
                reloadData(true);
            }
        }
    }

    public void orderByDatePublished(boolean sort) {
        mOrderByDatePublished = sort;
        reloadData(false); // Set boolean and refresh data
    }

    public boolean isRefreshing() {
        return mRefresh.isRefreshing();
    }

    @Override
    public void onRefresh() {
        reloadData(true);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        public void onListFragmentInteraction(Entry entry);
    }
}
