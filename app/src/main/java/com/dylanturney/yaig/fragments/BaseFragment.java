package com.dylanturney.yaig.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.ButterKnife;
import io.realm.Realm;
import rx.Subscription;

/**
 * Created by dylanturney on 16/11/2016.
 */

public class BaseFragment extends Fragment {

    protected final String TAG = this.getClass().getSimpleName();

    protected Context mContext;
    @LayoutRes
    protected int mLayoutId;
    protected boolean mUseButterKnife = false;
    protected final List<Subscription> mSubscriptions = new ArrayList<>();
    protected Realm mRealm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mUseButterKnife) {
            View view = inflater.inflate(mLayoutId, container, false);
            ButterKnife.bind(this, view);
            return view;
        } else {
            return super.onCreateView(inflater, container, savedInstanceState);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void onStop() {
        super.onStop();

        unsubscribe();

        if (mRealm != null) {
            mRealm.close();
        }
    }

    protected void unsubscribe() {
        for (Iterator<Subscription> iterator = mSubscriptions.iterator(); iterator.hasNext();){
            Subscription subscription = iterator.next();
            if (subscription != null && !subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
            iterator.remove();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mContext = null;
    }
}
