package com.dylanturney.yaig;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by dylanturney on 17/11/2016.
 */

public class YaigApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this); // Initialize Fresco

        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

}
